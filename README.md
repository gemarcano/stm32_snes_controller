
This project combines multiple STM32 repositories I've organized to make a
Super Nintendo Controller to USB adapter. It leverages TinyUSB for the USB
stack, and uses FreeRTOS internally.

The USB HID gamepad exposes 2 axis and 8 buttons.

# Dependencies

This project uses Meson to build everything, and it relies on all dependencies
being finable by using the dependencies() functionality in meson. I've set up a
series of respositories building the subcomponents and allowing them to install
to a given prefix with an accompanying pkg_config file, allowing the
dependencies mechanism to work normally.

Setting up meson
```
$ mkdir build; cd build
$ meson --prefix ~/.local/stm32l052k8/ ../ --cross-file ~/.local/stm32l052k8/share/cmsis5/stm32l052k8  --cross-file ../my_cross.txt
$ meson compile
```

my_cross.txt
```
[constants]
prefix = '/home/foobar/.local/stm32l052k8/'
```

Here are the other repositories that I used to help set everything up:
 - www.gitlab.com/gemarcano/stm32_cmsis_headers
 - www.gitlab.com/gemarcano/stm32_freertos
 - www.gitlab.com/gemarcano/stm32_stm32l0xx_hal_driver
