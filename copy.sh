#!/bin/sh

set -e

# Set this variable to the location of the tinyusb repository to use as a
# source
export TINYUSB=../tinyusb
export EXAMPLE=hid_composite_freertos

# Copy source files from tinyusb
echo "Copying tinyusb sources..."
mkdir -p ./src/tinyusb
rsync -rR $(find ${TINYUSB}/src/./ -type f -name "*.c") ${TINYUSB}/hw/./bsp/board.c ${TINYUSB}/hw/./bsp/stm32l052k8/stm32l052k8.c ./src/tinyusb/
cp ${TINYUSB}/examples/device/${EXAMPLE}/src/*.c ./src/
echo "done"

# Copy include files from tinyusb
echo "Copying tinyusb includes..."
mkdir -p ./include/tinyusb
rsync -rR $(find ${TINYUSB}/src/./ -type f -name "*.h") ${TINYUSB}/hw/./bsp/*.h ./include/tinyusb/
cp ${TINYUSB}/examples/device/${EXAMPLE}/src/*.h ./include/
echo "done"

echo "Applying fixups..."
# Fixup an include... relative include is not necessary
sed -i'' 's$#include "../board.h"$#include "board.h"$' ./src/tinyusb/bsp/stm32l052k8/stm32l052k8.c
patch -sp1 < tinyusb-warning-fix.patch
echo "done"
