#ifndef SNES_CONTROLLER_H_
#define SNES_CONTROLLER_H_

#include <stm32l052xx.h>
#include <stm32l0xx_hal.h>

#include <cstdint>
#include <span>

struct snes_controller_gpio
{
	GPIO_TypeDef *port;
	uint8_t pin;
};

class snes_controller_spi
{
public:
	snes_controller_spi(const std::span<const snes_controller_gpio, 2>& gpios);
	uint16_t read();

private:
	DMA_HandleTypeDef hdma_rx;
	DMA_HandleTypeDef hdma_tx;
	SPI_HandleTypeDef spi;
};

struct controller_state
{
	int8_t x;        // x axis, [-127, 127]
	int8_t y;        // y axis, [-127, 127]
	uint8_t buttons; // buttons, 8 buttons
	bool operator==(const controller_state&) const = default;
};

class snes_controller
{
public:
	snes_controller(const std::span<const snes_controller_gpio, 3>& gpios);
	controller_state read();
private:
	snes_controller_gpio latch;
	snes_controller_spi spi;
};

#endif//SNES_CONTROLLER_H_
