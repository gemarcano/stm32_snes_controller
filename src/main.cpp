// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <snes_controller.h>

#include <tinyusb/bsp/board.h>
#include <tusb.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include <queue.h>
#include <task.h>
#include <timers.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <exception>

// Just hang if we call a pure virtual function, the default implementation
// relies on too much stuff
extern "C" void __cxa_pure_virtual() { while(1); }

// Also just hang if we get to the __verbose_terminate_handler
namespace __gnu_cxx
{
	void __verbose_terminate_handler()
	{
		for (;;);
	}
}

// Establish resources used by FreeRTOS tasks
#define USB_STACK_SIZE (1000/sizeof(StackType_t))
StackType_t usb_device_stack[USB_STACK_SIZE];
StaticTask_t usb_device_taskdef;

#define HID_STACK_SIZE (800/sizeof(StackType_t))
StackType_t hid_stack[HID_STACK_SIZE];
StaticTask_t hid_taskdef;

// FreeRTOS task to handle USB tasks
void usb_device_task(void *)
{
	tusb_init();
	for(;;)
	{
		tud_task();
	}
}

// FreeRTOS task to handle polling controller and sending HID reports
void hid_task(void *)
{
	static const std::array<snes_controller_gpio, 3> gpios = {{
		{GPIOB, 3}, // CLK
		{GPIOB, 4}, // MISO
		{GPIOB, 5}, // MOSI/LATCH
	}};
	snes_controller controller{std::span(gpios)};

	TickType_t last = xTaskGetTickCount();
	controller_state last_data{};
	for (;;)
	{
		vTaskDelayUntil(&last, pdMS_TO_TICKS(10));
		if (!tud_hid_ready()) continue;

		controller_state data = controller.read();

		// Remote wakeup only if it's suspended, and a button is pressed
		if (tud_suspended() && (data.x || data.y || data.buttons))
		{
			// Host must allow waking up from this device for this to work
			tud_remote_wakeup();
		}

		// Only send a report if the data has changed
		else if (last_data != data)
		{
			// Our report only has 3 bytes, don't assume the struct with the data has
			// no padding, and don't use the no padding directive for structs-- last
			// thing I want to deal with is misaligned data access on ARM
			uint8_t buffer[3];
			buffer[0] = data.x;
			buffer[1] = data.y;
			buffer[2] = data.buttons;
			tud_hid_report(0, &buffer, sizeof(buffer));
			last_data = data;
		}
	}
}

// Invoked when received GET_REPORT control request
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request
uint16_t tud_hid_get_report_cb(uint8_t itf, uint8_t report_id, hid_report_type_t report_type, uint8_t* buffer, uint16_t reqlen)
{
	// TODO not Implemented
	(void) itf;
	(void) report_id;
	(void) report_type;
	(void) buffer;
	(void) reqlen;

	return 0;
}

// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 )
void tud_hid_set_report_cb(uint8_t itf, uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize)
{
	// This example doesn't use multiple report and report ID
	(void) itf;
	(void) report_id;
	(void) report_type;
	(void)buffer;
	(void)bufsize;
}

int main()
{
	board_init();

	xTaskCreateStatic(
		usb_device_task,
		"usbd",
		USB_STACK_SIZE,
		NULL,
		configMAX_PRIORITIES-1,
		usb_device_stack,
		&usb_device_taskdef);

	xTaskCreateStatic(
		hid_task,
		"hid",
		HID_STACK_SIZE,
		NULL,
		configMAX_PRIORITIES-2,
		hid_stack,
		&hid_taskdef);

	vTaskStartScheduler();
	return 0;
}
