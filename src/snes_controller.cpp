// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2022

#include <snes_controller.h>
#include <stm32l0xx_hal.h>
#include <stddef.h>

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include <span>

static StaticSemaphore_t dma_semaphore;
static SemaphoreHandle_t dma_semaphore_handle;

static SPI_HandleTypeDef *spi_handle;
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *spi)
{
	(void)spi;
	xSemaphoreGive(dma_semaphore_handle);
}

snes_controller_spi::snes_controller_spi(const std::span<const snes_controller_gpio, 2>& gpios)
{
	dma_semaphore_handle = xSemaphoreCreateBinaryStatic(&dma_semaphore);

	// Enable port clocks first, else no settings will stick
	for (size_t i = 0; i < 2; ++i)
	{
		if (GPIOA == gpios[i].port)
			__HAL_RCC_GPIOA_CLK_ENABLE();
		else if (GPIOB == gpios[i].port)
			__HAL_RCC_GPIOB_CLK_ENABLE();
		else if (GPIOC == gpios[i].port)
			__HAL_RCC_GPIOC_CLK_ENABLE();
	}

	__HAL_RCC_DMA1_CLK_ENABLE();
	__HAL_RCC_SPI1_CLK_ENABLE();

	// FIXME this assumes CLK and MISO are PB3 and PB4!
	GPIO_InitTypeDef gpio_config = {
		.Pin = 0, // Don't care, will set later
		.Mode = GPIO_MODE_AF_OD,
		.Pull = GPIO_NOPULL,
		.Speed = GPIO_SPEED_FREQ_HIGH,
		.Alternate = GPIO_AF0_SPI1,
	};

	// CLK
	gpio_config.Pin = 1 << gpios[0].pin;
	HAL_GPIO_Init(gpios[0].port, &gpio_config);

	// MISO
	gpio_config.Pin = 1 << gpios[1].pin;
	HAL_GPIO_Init(gpios[1].port, &gpio_config);

	// MOSI is latch, and that's set up by something else

	// Set up SPI DMAs
	spi_handle = &spi;

	hdma_rx.Init.Request = DMA_REQUEST_1;
	hdma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_rx.Init.MemInc = DMA_MINC_DISABLE;
	hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_rx.Init.Mode = DMA_NORMAL;
	hdma_rx.Init.Priority = DMA_PRIORITY_HIGH;
	hdma_rx.Instance = DMA1_Channel2; // Only channel2 can read SPI1 RX

	HAL_DMA_Init(&hdma_rx);
	__HAL_LINKDMA(&spi, hdmarx, hdma_rx);

	hdma_tx.Init.Request = DMA_REQUEST_1;
	hdma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_tx.Init.MemInc = DMA_MINC_DISABLE;
	hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_tx.Init.Mode = DMA_NORMAL;
	hdma_tx.Init.Priority = DMA_PRIORITY_HIGH; //???
	hdma_tx.Instance = DMA1_Channel3; // Only channel3 can do SPI1 TX

	HAL_DMA_Init(&hdma_tx);
	__HAL_LINKDMA(&spi, hdmatx, hdma_tx);

	HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

	// Finally set up SPI hardware
	spi.Instance = SPI1;
	spi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
	spi.Init.Direction = SPI_DIRECTION_2LINES;
	spi.Init.CLKPhase = SPI_PHASE_1EDGE;
	spi.Init.CLKPolarity = SPI_POLARITY_HIGH;
	spi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	spi.Init.CRCPolynomial = 7;
	spi.Init.DataSize = SPI_DATASIZE_16BIT;
	spi.Init.FirstBit = SPI_FIRSTBIT_LSB;
	spi.Init.NSS = SPI_NSS_SOFT;
	spi.Init.TIMode = SPI_TIMODE_DISABLE;
	spi.Init.Mode = SPI_MODE_MASTER;

	HAL_SPI_Init(&spi);
}

extern "C" void DMA1_Channel2_3_IRQHandler(void)
{
	HAL_DMA_IRQHandler(spi_handle->hdmarx);
	HAL_DMA_IRQHandler(spi_handle->hdmatx);
}

snes_controller::snes_controller(const std::span<const snes_controller_gpio, 3>& gpios)
:spi(std::span<const snes_controller_gpio, 2>(gpios.subspan<0, 2>()))
{
	// Enable port clocks first, else no settings will stick
	if (GPIOA == gpios[2].port)
		__HAL_RCC_GPIOA_CLK_ENABLE();
	else if (GPIOB == gpios[2].port)
		__HAL_RCC_GPIOB_CLK_ENABLE();
	else if (GPIOC == gpios[2].port)
		__HAL_RCC_GPIOC_CLK_ENABLE();

	latch = gpios[2];

	GPIO_InitTypeDef gpio_config = {
		.Pin = 0, // Don't care, will set later
		.Mode = GPIO_MODE_OUTPUT_OD,
		.Pull = GPIO_NOPULL,
		.Speed = GPIO_SPEED_FREQ_HIGH,
		.Alternate = 0, // Don't care
	};

	gpio_config.Pin = 1 << latch.pin;
	HAL_GPIO_WritePin(latch.port, 1 << latch.pin, GPIO_PIN_RESET);
	HAL_GPIO_Init(latch.port, &gpio_config);
}

controller_state snes_controller::read()
{
	HAL_GPIO_WritePin(latch.port, 1 << latch.pin, GPIO_PIN_SET);
	for (unsigned i = 0; i < 50; ++i)
	{
		asm volatile("nop");
	}
	HAL_GPIO_WritePin(latch.port, 1 << latch.pin, GPIO_PIN_RESET);

	uint16_t data = spi.read();

	uint8_t buttons =
		(data & 0xF) |
		(data & (0xF << 8)) >> 4;

	int8_t x = (!(data & (1 << 7))) - (!(data & (1 << 6)));
	int8_t y = (!(data & (1 << 5))) - (!(data & (1 << 4)));
	controller_state result =
	{
		.x = static_cast<int8_t>(x * 127), //6-7
		.y = static_cast<int8_t>(y * 127), //4-5
		.buttons = static_cast<uint8_t>(~buttons)
	};

	return result;
}

uint16_t snes_controller_spi::read()
{
	uint16_t data = 0;
	uint16_t dummy = 0xCACA;
	HAL_SPI_TransmitReceive_DMA(&spi, (uint8_t*)&dummy, (uint8_t*)&data, 1);
	while (HAL_SPI_GetState(&spi) != HAL_SPI_STATE_READY)
	{
		xSemaphoreTake(dma_semaphore_handle, portMAX_DELAY);
	}
	return data;
}
